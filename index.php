<?php
require __DIR__ . '/vendor/autoload.php';
require 'libs/NotORM.php';
use \Slim\App;

$app = new App();

// Konfigurasi Server dan Database
$dbhost = '127.0.0.1';
$dbuser = 'admin';
$dbpass = 'admin123';
$dbname = 'sample_db_api';
$dbmethod = 'mysql:dbname=';

$dsn = $dbmethod.$dbname;
$pdo = new PDO($dsn, $dbuser, $dbpass);
$db = new NotORM($pdo);

$app-> get('/', function(){
  echo "SLIM API";
});

// get semua data.
$app->get('/semuadosen', function() use($app, $db){
  $dosen["error"] = false;
  $dosen["message"] = "Berhasil mendapatkan data dosen";
  foreach ($db->tbl_dosen() as $data) {
    $dosen['semuadosen'][] = array(
      'id' => $data['id'],
      'nama' => $data['nama'],
      'matkul' => $data['matkul']
    );
  }
  echo json_encode($dosen);
});

// get data by Nama
$app->get('/dosen/{nama}', function($request, $response, $args) use($app, $db){
  $dosen = $db->tbl_dosen()->where('nama', $args['nama']);
  $dosendetail = $dosen->fetch();

  if ($dosen->count() == 0) {
    $responseJson["error"] = true;
    $responseJson["message"] = "Nama dosen belum tersedia di database";
    $responseJson["nama"] = null;
    $responseJson["matkul"] = null;
    $responseJson["no_hp"] = null;
  }else {
    $responseJson["error"] = false;
    $responseJson["message"] = "Berhasil mengambil data";
    $responseJson["nama"] = $dosendetail['nama'];
    $responseJson["matkul"] = $dosendetail['matkul'];
    $responseJson["no_hp"] = $dosendetail['no_hp'];
  }
  echo json_encode($responseJson);
});

// for add data to Database.
$app->post('/matkul', function($request, $response, $args) use($app, $db){
  $matkul = $request->getParams();
  $result = $db->tbl_matkul()->insert($matkul);

  $responseJson["error"] = false;
  $responseJson["message"] = "Berhasil menambahkan ke database";

  echo json_encode($responseJson);
});

<<<<<<< HEAD
// untuk update Database
$app->put('/update/{id}', function($request, $response, $args) use($app, $db){
  $update = $db->tbl_matkul()->where('id', $args);
  if ($update->fetch()) {
    $post = $request->getParams();
    $result = $update->update($post);

    echo json_encode(array(
      "status" => (bool) $result,
      "message" => "Data sudah diupdate"
    ));

  }else {
    echo json_encode(array(
      "status" => false,
      "message" => "Gagal update data"
    ));

  }
});
// untuk hapus data. sample : http://localhost/slim_api/matkul/8
=======
// for delete data. sample : http://localhost/slim_api/matkul/8
>>>>>>> b1073d815c229806dc35dacf68243decbde90e50
$app->delete('/matkul/{id}', function($request, $response, $args) use($app, $db){
  $matkul = $db->tbl_matkul()->where('id', $args);
  if ($matkul->fetch()) {
    $result = $matkul->delete();
    echo json_encode(array(
      "error" => false,
      "message" => "Matkul berhasil dihapus"));
  }else {
    echo json_encode(array(
      "error" => true,
      "message" => "Matkul ID tersebut tidak ada"));
  }

});
//run App
$app->run();
 ?>
